libspectrum (1.5.0-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Bump debhelper from old 12 to 13.
  * Set field Upstream-Name in debian/copyright.
  * Update standards version to 4.6.1, no changes needed.

  [ Alberto Garcia ]
  * Use secure URI in debian/copyright and debian/watch.
  * debian/rules:
    - Explicitly disable LTO (Closes: #1015523).
  * debian/patches/hide-internal-symbols.patch:
    - Update to hide an internal symbol used by the tests.
  * debian/libspectrum8.symbols:
    - Update after the removal of the internal symbol by the previous patch.
  * debian/copyright:
    - Update copyright years.

 -- Alberto Garcia <berto@igalia.com>  Tue, 10 Jan 2023 11:19:20 +0100

libspectrum (1.5.0-3) unstable; urgency=medium

  * debian/patches/fix-big-endian-build.patch:
    - Fix build in big-endian machines.

 -- Alberto Garcia <berto@igalia.com>  Mon, 16 Aug 2021 22:10:02 +0200

libspectrum (1.5.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Alberto Garcia <berto@igalia.com>  Mon, 16 Aug 2021 17:38:15 +0200

libspectrum (1.5.0-1) experimental; urgency=medium

  * New upstream release.
  * Remove the upstream PGP signing key, the libspectrum 1.5.0 tarball is
    not signed.
  * debian/patches/revert-soname-change.patch:
    - Upstream bumped the API version of the library from 8 to 9. This is
      unnecessary in the case of the Debian build so this patch reverts
      the soname change.
  * debian/patches/hide-internal-symbols.patch:
    - Export only the symbols that are part of the public API.
  * debian/patches/run-tests.patch:
    - Run the tests during 'make check'.
  * debian/copyright:
    - Update copyright years.
  * debian/control:
    - Add Rules-Requires-Root: no.
    - Update Standards-Version to 4.5.1 (no changes).
  * debian/watch:
    - Set version to 4 (fixes older-debian-watch-file-standard)
  * debian/libspectrum8.symbols:
    - Replace libspectrum8.shlibs with libspectrum8.symbols.
  * Add upstream metadata file.
  * debian/rules:
    - Build with hardening=+bindnow.
  * Set the debhelper compatibility level to 12:
    - Get rid of debian/compat.
    - Add build dependency on debhelper-compat.
  * debian/not-installed:
    - Add libspectrum.la.

 -- Alberto Garcia <berto@igalia.com>  Wed, 03 Mar 2021 10:49:26 +0100

libspectrum (1.4.4-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Update Standards-Version to 4.1.5 (no changes).

 -- Alberto Garcia <berto@igalia.com>  Thu, 05 Jul 2018 21:55:25 +0200

libspectrum (1.4.3-1) unstable; urgency=medium

  * New upstream release.
  * debian/libspectrum8.shlibs:
    - Bump shlibs to 1.4.3.
  * debian/control:
    - Add Vcs-* fields.

 -- Alberto Garcia <berto@igalia.com>  Wed, 06 Jun 2018 11:44:33 +0300

libspectrum (1.4.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Update Standards-Version to 4.1.4 (no changes).
  * Set debhelper compatibility level to v10:
    - debian/control: update build dependency on debhelper and remove
      build dependency on dh-autoreconf.
    - debian/compat: update compatibility level.
    - debian/rules: remove '--with autoreconf --parallel', these are now
      enabled by default.
  * debian/copyright:
    - Update copyright years.

 -- Alberto Garcia <berto@igalia.com>  Wed, 02 May 2018 09:42:00 +0300

libspectrum (1.4.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Update Standards-Version to 4.1.1 (no changes).

 -- Alberto Garcia <berto@igalia.com>  Wed, 11 Oct 2017 11:56:23 +0300

libspectrum (1.4.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright:
    - Use https for the Format URL.
  * debian/control:
    - Update Standards-Version to 4.1.0 (no changes).
  * debian/libspectrum8.shlibs:
    - Bump shlibs to 1.4.0.

 -- Alberto Garcia <berto@igalia.com>  Tue, 05 Sep 2017 16:43:01 +0300

libspectrum (1.3.6-2) unstable; urgency=medium

  * libspectrum-dev.install:
    - Install the libspectrum.pc file.

 -- Alberto Garcia <berto@igalia.com>  Thu, 03 Aug 2017 12:37:27 +0300

libspectrum (1.3.6-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Update Standards-Version to 4.0.0 (no changes).

 -- Alberto Garcia <berto@igalia.com>  Wed, 02 Aug 2017 11:28:19 +0300

libspectrum (1.3.5-1) unstable; urgency=medium

  * New upstream release.
  * debian/libspectrum8.shlibs:
    - Bump shlibs to 1.3.5.

 -- Alberto Garcia <berto@igalia.com>  Thu, 13 Jul 2017 01:10:35 +0200

libspectrum (1.3.4-2) unstable; urgency=medium

  * Upload to unstable.
  * debian/libspectrum8.shlibs:
    - Bump shlibs to 1.3.4.

 -- Alberto Garcia <berto@igalia.com>  Tue, 20 Jun 2017 12:35:47 +0300

libspectrum (1.3.4-1) experimental; urgency=medium

  * New upstream release.

 -- Alberto Garcia <berto@igalia.com>  Fri, 02 Jun 2017 11:52:10 +0300

libspectrum (1.3.3-1) experimental; urgency=medium

  * New upstream release.
  * debian/copyright:
    - Update copyright years.

 -- Alberto Garcia <berto@igalia.com>  Sun, 30 Apr 2017 14:52:05 +0300

libspectrum (1.3.0-1) unstable; urgency=medium

  * New upstream release.

 -- Alberto Garcia <berto@igalia.com>  Mon, 03 Oct 2016 09:54:58 +0300

libspectrum (1.2.2-1) unstable; urgency=medium

  * New upstream release.

 -- Alberto Garcia <berto@igalia.com>  Tue, 23 Aug 2016 08:28:54 -0400

libspectrum (1.2.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Replace dependency on libgcrypt11-dev with libgcrypt20-dev.

 -- Alberto Garcia <berto@igalia.com>  Wed, 20 Jul 2016 02:01:20 +0200

libspectrum (1.2.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/libspectrum8.shlibs:
    - Bump shlibs to 1.2.0.
  * debian/control:
    - Update Standards-Version to 3.9.8 (no changes).
    - Update the list of supported formats in the description.
  * debian/copyright:
    - Update copyright years.
  * debian/rules:
    - Enable parallel builds.
  * Verify GPG signature of the upstream tarball:
    - debian/upstream/signing-key.asc: Add key file.
    - debian/watch: Add pgpsigurlmangle option.

 -- Alberto Garcia <berto@igalia.com>  Tue, 07 Jun 2016 13:59:42 +0300

libspectrum (1.1.1-1) unstable; urgency=low

  * New upstream release.
  * Update my e-mail address in debian/control and debian/copyright.
  * Remove audiofile_0.3.patch, it is already included in this release.
  * debian/libspectrum8.shlibs: bump shlibs to 1.1.1.
  * debian/control:
    - Remove obsolete DM-Upload-Allowed flag.
    - Update Standards-Version to 3.9.4 (no changes).
    - Update description to reflect the new supported formats.
  * debian/copyright: update copyright years.

 -- Alberto Garcia <berto@igalia.com>  Mon, 03 Jun 2013 00:06:13 +0200

libspectrum (1.0.0-3) unstable; urgency=low

  * Add support for libaudiofile 0.3:
    - audiofile_0.3.patch: detect libaudiofile using pkg-config, recent
      versions no longer ship audiofile-config.
    - debian/control: add build dependency on dh-autoreconf.
    - debian/rules: run dh --with autoreconf.
  * Multi-arch support:
    - debian/compat: set compatibility level to 9.
    - debian/control: build depend on debhelper >= 9.
    - debian/control: Add Multi-Arch and Pre-Depends fields.
    - debian/libspectrum{8,-dev}.install: replace usr/lib/ with usr/lib/*/.
  * debian/control: update Standards-Version to 3.9.3.
  * debian/copyright: rewrite using the machine-readable format, and
    change the license of the packaging to GPL-2+.

 -- Alberto Garcia <agarcia@igalia.com>  Tue, 20 Mar 2012 12:39:15 +0200

libspectrum (1.0.0-2) unstable; urgency=low

  * debian/control: add DM-Upload-Allowed flag.
  * debian/libspectrum-dev.install: don't install .la files.

 -- Alberto Garcia <agarcia@igalia.com>  Tue, 22 Feb 2011 22:47:39 +0200

libspectrum (1.0.0-1) unstable; urgency=low

  * Initial release (Closes: #608200).

 -- Alberto Garcia <agarcia@igalia.com>  Tue, 28 Dec 2010 18:08:28 +0100
